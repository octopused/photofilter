//
//  PhotoFilterCaptureProcessor.swift
//  PhotoFilter
//
//  Created by RuslanKa on 24/09/2019.
//  Copyright © 2019 RuslanKa. All rights reserved.
//

import Foundation
import AVFoundation
import UIKit

class PhotoFilterCaptureProcessor: NSObject, AVCapturePhotoCaptureDelegate {
    
    var filter: CIFilter?
    var ciContext: CIContext?
    
    func photoOutput(_ output: AVCapturePhotoOutput, willBeginCaptureFor resolvedSettings: AVCaptureResolvedPhotoSettings) {
        
    }
    
    func photoOutput(_ output: AVCapturePhotoOutput, willCapturePhotoFor resolvedSettings: AVCaptureResolvedPhotoSettings) {
        
    }
    
    func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?) {
        if let error = error {
            fatalError(error.localizedDescription)
        }
        guard let data = photo.fileDataRepresentation(),
            let ciContext = ciContext else {
            return
        }
        if let filter = filter,
            let image = UIImage(data: data),
            let ciImage = CIImage(image: image),
            let filteredImage = ciImage.filter(by: filter, in: ciContext, orientation: .leftMirrored) {
            UIImageWriteToSavedPhotosAlbum(filteredImage, nil, nil, nil)
        } else if let ciImage = CIImage(data: data),
            let cgImage = ciContext.createCGImage(ciImage, from: ciImage.extent) {
            let image = UIImage(cgImage: cgImage, scale: 1, orientation: .leftMirrored)
            UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
        }
    }
    
    func photoOutput(_ output: AVCapturePhotoOutput, didFinishCaptureFor resolvedSettings: AVCaptureResolvedPhotoSettings, error: Error?) {
        
    }
}
