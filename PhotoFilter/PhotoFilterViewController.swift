//
//  ViewController.swift
//  PhotoFilter
//
//  Created by RuslanKa on 24/09/2019.
//  Copyright © 2019 RuslanKa. All rights reserved.
//

import UIKit
import AVFoundation
import PhotosUI
import AssetsLibrary

final class PhotoFilterViewController: UIViewController {

    // MARK: - Properties
    var output: PhotoFilterViewOutput?
    var photoCaptureProcessor: PhotoFilterCaptureProcessor?
    
    let captureSession = AVCaptureSession()
    let photoOutput = AVCapturePhotoOutput()
    let videoOutput = AVCaptureVideoDataOutput()
    let ciContext = CIContext()
    
    var filters = [
        nil,
        CIFilter(name: "CIGaussianBlur"),
        CIFilter(name: "CIComicEffect"),
        CIFilter(name: "CICrystallize"),
    ]
    var filterIndex = 0
    var currentFilter: CIFilter? {
        if filterIndex < 0 || filterIndex >= filters.count {
            return nil
        }
        return filters[filterIndex]
    }
    
    let buttonWidthConstant = CGFloat(70)
    
    @IBOutlet weak var cameraPreviewView: CameraPreviewView!
    @IBOutlet weak var takePhotoButton: UIButton!
    @IBOutlet weak var takePhotoButtonWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var imageView: UIImageView!
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        output?.viewDidLoad()
    }
    
    // MARK: - Private methods
    private func setupUI() {
        takePhotoButton.addTarget(self, action: #selector(startButtonAnimation), for: .touchDown)
        takePhotoButton.addTarget(self, action: #selector(endButtonAnimation), for: .touchUpInside)
        takePhotoButton.addTarget(self, action: #selector(takePhoto), for: .touchUpInside)
        cameraPreviewView.cameraPreviewLayer.session = captureSession
        
        imageView.isHidden = true
        cameraPreviewView.isHidden = false
        
        let leftSwipe = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipe(swipeGesture:)))
        leftSwipe.direction = .left
        let rightSwipe = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipe(swipeGesture:)))
        rightSwipe.direction = .right
        view.addGestureRecognizer(leftSwipe)
        view.addGestureRecognizer(rightSwipe)
    }
    
    @objc private func takePhoto() {
        guard let photoCaptureProcessor = photoCaptureProcessor,
            let currentFilter = currentFilter else {
            return
        }
        let photoSettings = AVCapturePhotoSettings()
        photoSettings.flashMode = .off
        photoCaptureProcessor.filter = currentFilter
        photoOutput.capturePhoto(with: photoSettings, delegate: photoCaptureProcessor)
    }
    
    @objc private func startButtonAnimation() {
        takePhotoButtonWidthConstraint.constant = buttonWidthConstant * 1.5
        UIView.animate(withDuration: 0.3, animations: { [weak self] in
            self?.takePhotoButton.layer.cornerRadius = (self!.buttonWidthConstant * 1.5) / 2
            self?.view.layoutSubviews()
        })
    }
    
    @objc private func endButtonAnimation() {
        takePhotoButtonWidthConstraint.constant = buttonWidthConstant
        UIView.animate(withDuration: 0.3, animations: { [weak self] in
            self?.takePhotoButton.layer.cornerRadius = self!.buttonWidthConstant / 2
            self?.view.layoutSubviews()
        })
    }
    
    @objc private func handleSwipe(swipeGesture: UISwipeGestureRecognizer) {
        switch swipeGesture.direction {
        case .right:
            filterIndex = filterIndex + 1
        case .left:
            filterIndex = filterIndex - 1
        default:
            break
        }
        
        if filterIndex >= filters.count {
            filterIndex = 0
        } else if filterIndex < 0 {
            filterIndex = filters.count - 1
        }
        
        if filters[filterIndex] == nil {
            videoOutput.setSampleBufferDelegate(nil, queue: nil)
            imageView.isHidden = true
            cameraPreviewView.isHidden = false
        } else {
            videoOutput.setSampleBufferDelegate(self, queue: .main)
            imageView.isHidden = false
            cameraPreviewView.isHidden = true
        }
    }
    
}

// MARK: - Input
extension PhotoFilterViewController: PhotoFilterViewInput {
    
    func checkCameraAccess() {
        switch AVCaptureDevice.authorizationStatus(for: .video) {
        case .authorized:
            output?.cameraAccess(granted: true)
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: .video) { [weak self] (granted) in
                self?.output?.cameraAccess(granted: granted)
            }
        case .denied, .restricted:
            output?.cameraAccess(granted: false)
        @unknown default:
            output?.cameraAccess(granted: false)
        }
    }
    
    func setupPhotoSession() {
        captureSession.beginConfiguration()
        
        // Photo input
        guard let frontCamera = AVCaptureDevice.default(.builtInWideAngleCamera, for: .video, position: .front),
            let deviceInput = try? AVCaptureDeviceInput(device: frontCamera),
            captureSession.canAddInput(deviceInput) else {
            return
        }
        captureSession.addInput(deviceInput)
        
        // Photo output
        guard captureSession.canAddOutput(photoOutput) else {
            return
        }
        captureSession.sessionPreset = .photo
        captureSession.addOutput(photoOutput)
        
        // Video output
        videoOutput.setSampleBufferDelegate(self, queue: .main)
        captureSession.addOutput(videoOutput)
        
        photoCaptureProcessor?.ciContext = ciContext
        
        captureSession.commitConfiguration()
        output?.captureSessionConfigured()
    }
    
    func startPhotoSession() {
        captureSession.startRunning()
    }
}

extension PhotoFilterViewController: AVCaptureVideoDataOutputSampleBufferDelegate {
    func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        guard let pixelBuffer = CMSampleBufferGetImageBuffer(sampleBuffer),
        let filter = filters[filterIndex] else {
            return
        }
        let cameraImage = CIImage(cvPixelBuffer: pixelBuffer)
        imageView.image = cameraImage.filter(by: filter, in: ciContext, orientation: .leftMirrored)
    }
}
