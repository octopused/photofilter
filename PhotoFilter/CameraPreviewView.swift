//
//  CameraPreviewView.swift
//  PhotoFilter
//
//  Created by RuslanKa on 24/09/2019.
//  Copyright © 2019 RuslanKa. All rights reserved.
//

import AVFoundation
import UIKit

class CameraPreviewView: UIView {

    override class var layerClass: AnyClass {
        return AVCaptureVideoPreviewLayer.self
    }

    var cameraPreviewLayer: AVCaptureVideoPreviewLayer {
        return layer as! AVCaptureVideoPreviewLayer
    }
    
}
