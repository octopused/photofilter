//
//  PhotoFilterProtocols.swift
//  PhotoFilter
//
//  Created by RuslanKa on 24/09/2019.
//  Copyright © 2019 RuslanKa. All rights reserved.
//

import Foundation

protocol PhotoFilterViewInput {
    func checkCameraAccess()
    func setupPhotoSession()
    func startPhotoSession()
}

protocol PhotoFilterViewOutput {
    func viewDidLoad()
    func captureSessionConfigured()
    func cameraAccess(granted: Bool)
}

protocol PhotoFilterModuleInput {
    
}

protocol PhotoFilterModuleOutput {
    
}
