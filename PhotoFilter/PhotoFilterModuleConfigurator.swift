//
//  PhotoFilterModule.swift
//  PhotoFilter
//
//  Created by RuslanKa on 24/09/2019.
//  Copyright © 2019 RuslanKa. All rights reserved.
//

import Foundation
import UIKit

class PhotoFilterModuleConfigurator {
    func configure(with moduleOutput: PhotoFilterModuleOutput? = nil) -> UIViewController {
        let storyboard = UIStoryboard(name: "PhotoFilter", bundle: nil)
        guard let viewController = storyboard.instantiateViewController(identifier: String(describing: PhotoFilterViewController.self)) as? PhotoFilterViewController else {
            fatalError("Could not load PhotoFilterViewController from storyboard")
        }
        
        let presenter = PhotoFilterPresenter()
        presenter.moduleOutput = moduleOutput
        presenter.view = viewController
        viewController.output = presenter
        viewController.photoCaptureProcessor = PhotoFilterCaptureProcessor()
        
        return viewController
    }
}
