//
//  CIImage+Extensions.swift
//  PhotoFilter
//
//  Created by RuslanKa on 25/09/2019.
//  Copyright © 2019 RuslanKa. All rights reserved.
//

import UIKit

extension CIImage {
    func filter(by filter: CIFilter, in context: CIContext, orientation: UIImage.Orientation) -> UIImage? {
        filter.setValue(self, forKey: kCIInputImageKey)
        guard let ciImage = filter.outputImage,
            let cgImage = context.createCGImage(ciImage, from: ciImage.extent) else {
            return nil
        }
        return UIImage(cgImage: cgImage, scale: 1, orientation: orientation)
    }
}
