//
//  PhotoFilterPresenter.swift
//  PhotoFilter
//
//  Created by RuslanKa on 24/09/2019.
//  Copyright © 2019 RuslanKa. All rights reserved.
//

import Foundation

class PhotoFilterPresenter {
    var moduleOutput: PhotoFilterModuleOutput?
    var view: PhotoFilterViewInput?
}

extension PhotoFilterPresenter: PhotoFilterViewOutput {
    
    func viewDidLoad() {
        view?.checkCameraAccess()
    }
    
    func cameraAccess(granted: Bool) {
        view?.setupPhotoSession()
    }
    
    func captureSessionConfigured() {
        view?.startPhotoSession()
    }
    
    func cameraOrPhotoLibraryAccessDenied() {
        
    }
    
}
